# xenia-overlay

Portage overlay for Xenia Linux.

List of Maintainers:\
Mia Neufeld - mia@xenialinux.com - Head Maintainer \
Luna Deards luna@xenialinux.com \
Jack Eilles jack@xenialinux.com 

How to install:

```sh
eselect repository add xenia-overlay git https://gitlab.com/xenia-group/xenia-overlay.git
emaint sync --repo xenia-overlay
```
